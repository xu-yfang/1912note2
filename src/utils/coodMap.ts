const statusCodeMap: any = {
    "400": "您的参数有误",
    "401": "您的身份当前不正确，或者登录过期，请重新登录",
    "403": "抱歉，暂无权限",
    "404": "找不到页面",
    "500": "服务器异常，请联系工作人员",
  }
  
  export default statusCodeMap
  