
import http from '@/utils/httpTool'

export const login = (data: any) => http.post("/api/auth/login",data)

// 知识小册数据
export const knowledgeList = () => http.post("/api/view")