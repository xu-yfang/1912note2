import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router';
import BasicLayout from '../layouts/BasicLayout.vue';

// only githubpages preview site used, if use template please remove this check
// and use `createWebHistory` is recommend
const hasGithubPages = import.meta.env.VITE_GHPAGES;

export default createRouter({
  history: hasGithubPages ? createWebHashHistory() : createWebHistory(),
  routes: [
    {
      path:"/login",
      name:"login",
      meta: {"title": '登录',showMenuItem: false},
      component: () => import("@/views/login/index.vue")
    },
    {
      path: '/',
      name: 'index',
      meta: { title: 'Home',showMenuItem: false },
      component: BasicLayout,
      redirect: '/worker',
      children: [
        {
          path: '/worker',
          name: 'worker',
          meta: { title: '工作台', icon: 'icon-icon-test' },
          component: () => import("@/views/worker/workerPage.vue"),
        },
        {
          path: '/404',
          name: '404',
          meta: { title: '页面找不到', icon: 'icon-icon-test',showMenuItem: false },
          component: () => import("@/views/notFound.vue"),
        },
        // {
        //   path: '/article',
        //   name: 'article',
        //   meta: { title: '文章管理', icon: 'icon-tuijian', flat: true },
        //   component: BlankLayout,
        //   // component: () => import("../views/article/index.vue"),
        //   redirect: () => ({ name: 'article' }),
        //   children: [
        //     {
        //       path: 'article',
        //       name: 'article',
        //       meta: { title: '所有文章' },
        //       // component: () => import('../views/article/PageInfo.vue'),
        //       component: () => import("@/views/article/index.vue"),
        //     },
        //     {
        //       path: 'category',
        //       name: 'category',
        //       meta: { title: '分类管理' },
        //       component: () => import('@/views/article/category.vue'),
        //     },
        //     {
        //       path: 'tags',
        //       name: 'tags',
        //       // 路由 path 默认参数再 meta.params 里
        //       meta: { title: '标签管理', params: { id: 1 } },
        //       component: () => import('@/views/article/tags.vue'),
        //     },
        //   ],
        // },
        // {
        //   path: '/path',
        //   name: 'path',
        //   meta: { title: '页面管理', icon: 'icon-antdesign' },
        //   component: () => import('@/views/path/index.vue'),
        // },
        // {
        //   path: '/knowledge',
        //   name: 'knowledge',
        //   meta: { title: '知识小册', icon: 'icon-antdesign' },
        //   component: () => import('@/views/knowledge/index.vue'),
        // },
        // {
        //   path: '/poster',
        //   name: 'poster',
        //   meta: { title: '海报管理', icon: 'icon-antdesign' },
        //   component: () => import('@/views/poster/index.vue'),
        // },
        // {
        //   path: '/comment',
        //   name: 'comment',
        //   meta: { title: '评论管理', icon: 'icon-antdesign' },
        //   component: () => import('@/views/comment/index.vue'),
        // },
        // {
        //   path: '/mail',
        //   name: 'mail',
        //   meta: { title: '邮箱管理', icon: 'icon-antdesign' },
        //   component: () => import('@/views/mail/index.vue'),
        // },
        // {
        //   path: '/file',
        //   name: 'file',
        //   meta: { title: '文件管理', icon: 'icon-antdesign' },
        //   component: () => import('@/views/file/index.vue'),
        // },
        // {
        //   path: '/search',
        //   name: 'search',
        //   meta: { title: '搜素纪录', icon: 'icon-antdesign' },
        //   component: () => import('@/views/search/index.vue'),
        // },
        // {
        //   path: '/view',
        //   name: 'view',
        //   meta: { title: '访问统计', icon: 'icon-antdesign' },
        //   component: () => import('@/views/view/index.vue'),
        // },
        // {
        //   path: '/user',
        //   name: 'user',
        //   meta: { title: '用户管理', icon: 'icon-antdesign' },
        //   component: () => import('@/views/user/index.vue'),
        // },
        // {
        //   path: '/setting',
        //   name: 'setting',
        //   meta: { title: '系统设置', icon: 'icon-antdesign' },
        //   component: () => import('@/views/setting/index.vue'),
        // },








      ],
    },
  ],
});
