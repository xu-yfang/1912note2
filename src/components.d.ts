// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/vue-next/pull/3399

declare module 'vue' {
  export interface GlobalComponents {
    AButton: typeof import('ant-design-vue/es')['Button']
    ADropdown: typeof import('ant-design-vue/es')['Dropdown']
    AImage: typeof import('ant-design-vue/es')['Image']
    AMenu: typeof import('ant-design-vue/es')['Menu']
    AMenuItem: typeof import('ant-design-vue/es')['MenuItem']
    AResult: typeof import('ant-design-vue/es')['Result']
    ASpace: typeof import('ant-design-vue/es')['Space']
    RightContent: typeof import('./components/RightContent/RightContent.vue')['default']
    SettingDrawer: typeof import('./components/SettingDrawer/SettingDrawer.vue')['default']
  }
}

export { }
